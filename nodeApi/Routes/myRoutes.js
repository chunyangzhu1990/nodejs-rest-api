////////////////////////////////////////////////////////////////////////////////
//
//  Author: Chunyang Zhu
//
//  Description: This file is setting up all the api routes
//               including requesting data from third party API.
//
//
////////////////////////////////////////////////////////////////////////////////
var express = require('express');
var request = require('request');
var myrouter = function () {

    var router = express.Router();

//Vehicle Info
//GET /vehicles/:id

    //setting up a route for the api
    router.route('/vehicles/:id').get(function (req, res) {
        // get the id number from the api url
        var myJSONObject = {
            "id": req.params.id,
            "responseType": "JSON"
        };
        // request info from GM api
        request({
            url: "http://gmapi.azurewebsites.net/getVehicleInfoService",
            method: "POST",
            json: true,
            body: myJSONObject
        }, function (error, response, body) {
        // get the data and package to the format we need

            var door;
            if (body.data.fourDoorSedan.value == "True")
                door = 4;
            if (body.data.twoDoorCoupe.value == "True")
                door = 2;

            var responseJson = {
                vin: body.data.vin.value,
                color: body.data.color.value,
                doorCount: door,
                driveTrain: body.data.driveTrain.value
            };
        // check if there is a error
            if (error)
                res.send(error);
            else
                res.json(responseJson);
        });

    });

//Security
//GET /vehicles/:id/doors


    //setting up a route for the api
    router.route('/vehicles/:id/doors').get(function (req, res) {
        // get the id number from the api url
        var myJSONObject = {
            "id": req.params.id,
            "responseType": "JSON"
        };
        // request info from GM api
        request({
            url: "http://gmapi.azurewebsites.net/getSecurityStatusService",
            method: "POST",
            json: true,
            body: myJSONObject
        }, function (error, response, body) {
        // get the data and package to the format we need
            var responseJson = [];

            for(var i = 0; i<body.data.doors.values.length;i++){
                 if (body.data.doors.values[i].locked.value == "True")
                responseJson.push({location: body.data.doors.values[i].location.value, locked:true});
            }

       // check if there is a error
            if (error)
                res.send(error);
            else
                res.json(responseJson);

        });
    });


//Fuel Range

//GET /vehicles/:id/fuel

    //setting up a route for the api
    router.route('/vehicles/:id/fuel').get(function (req, res) {
     // get the id number from the api url
        var myJSONObject = {
            "id": req.params.id,
            "responseType": "JSON"
        };
     // request info from GM api
        request({
            url: "http://gmapi.azurewebsites.net/getEnergyService",
            method: "POST",
            json: true,
            body: myJSONObject
        }, function (error, response, body) {
     // get the data and package to the format we need
            var responseJson =
            {
                "percent": parseInt(body.data.tankLevel.value)
            };
    // check if there is a error
            if (error)
                res.send(error);
            else
                res.json(responseJson);

        });
    });

//Battery Range
//GET /vehicles/:id/battery

  //setting up a route for the api
    router.route('/vehicles/:id/battery').get(function (req, res) {
  // get the id number from the api url
        var myJSONObject = {
            "id": req.params.id,
            "responseType": "JSON"
        };
   // request info from GM api
        request({
            url: "http://gmapi.azurewebsites.net/getEnergyService",
            method: "POST",
            json: true,
            body: myJSONObject
        }, function (error, response, body) {
      // get the data and package to the format we need
            var responseJson =
            {"percent": parseInt(body.data.batteryLevel.value)};
    // check if there is a error
            if (error)
                res.send(error);
            else
                res.json(responseJson);

        });
    });


//Start/Stop Engine
//POST /vehicles/:id/engine

  //setting up a route for the api
    router.route('/vehicles/:id/engine').post(function (req, res) {
   // check the command
        var command;
        if (req.body.action == "START")
            command = "START_VEHICLE";
        else
            command = "STOP_VEHICLE";

        var myJSONObject = {
            "id": req.params.id,
            "command": command,
            "responseType": "JSON"
        };
       // request info from GM api
        request({
            url: "http://gmapi.azurewebsites.net/actionEngineService",
            method: "POST",
            json: true,
            body: myJSONObject
        }, function (error, response, body) {
        // get the data and package to the format we need
            var result;
            if(body.actionResult.status=="EXECUTED")
                result="success";
            if(body.actionResult.status=="FAILED")
                result="error";

            var responseJson = {
                "status": result
            };
            res.json(responseJson);
        });
    });

    return router;
};
//export the routes
module.exports = myrouter;