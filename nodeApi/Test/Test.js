////////////////////////////////////////////////////////////////////////////////
//
//  Author: Chunyang Zhu
//
//  Description: This file is setting up all the Unit Test
//               using supertest, five tests test all the api we have.
//               The testing port is 8000, and the id number is 1234
//
////////////////////////////////////////////////////////////////////////////////

//Unit Testing using super test
var supertest = require("supertest");
var should = require("should");
var server = supertest.agent("http://localhost:8000");


//Testing Vehicle Info, require returning a 200 status & json contains four properties
describe('Vehicle Info Test', function () {
    it('should get the information of vehicles ', function (done) {
        server.get('/vehicles/1234')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, results) {
                results.body.should.have.property('vin');
                results.body.should.have.property('color');
                results.body.should.have.property('doorCount');
                results.body.should.have.property('driveTrain');
                done()
            })
    })
})

//Testing Security Info, require returning a 200 status & json array contains eight properties

describe('Security Test', function () {
    it('should get the Security status of vehicles ', function (done) {
        server.get('/vehicles/1234/doors')
            .expect('Content-Type', /json/)
            .expect(200)
            done();
    })
})

//Testing Fuel Range , require returning a 200 status & json  contains one property
describe('Fuel Range Test', function () {
    it('should get the Fuel Range of vehicles', function (done) {
        server.get('/vehicles/1234/fuel')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, results) {
                results.body.should.have.property('percent');
                done()
            })

    })
})

//Testing Battery Range , require returning a 200 status & json contains one property

describe('Battery Range Test', function () {
    it('should get the Battery of vehicles', function (done) {
        server.get('/vehicles/1234/battery')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, results) {
                results.body.should.have.property('percent');
                done()
            })
    })
})


//Testing Start/Stop Engine  , require returning a 200 status & json contains one property

describe('Start/Stop Engine test', function () {
    it('should allow engine "action": "START|STOP" Post, return a action status', function (done) {
        var enginePost = {"action": "START"};
        server.post('/vehicles/1234/engine')
            .send(enginePost)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, results) {
                results.body.should.have.property('status');
                done()
            })


    })
})