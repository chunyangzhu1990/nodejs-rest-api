////////////////////////////////////////////////////////////////////////////////
//
//  Author: Chunyang Zhu
//
//  Description: This file is setting up all the required parts for the server
//               including express, bodyparser, port, router.
//
//
////////////////////////////////////////////////////////////////////////////////
// set up node and express
var express = require('express');

// create a instance of express
var app = express();

// set up a port using a environment process, if environment is not set up, we use 3000
var port = process.env.PORT || 3000;

var bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//use a router control the routes make the code look clean
router = require('./Routes/myRoutes')();

app.use(router);

//build a test route of the site
app.get('/', function (req, res) {
    res.send('Welcome to my api ');
});

// listening a port
app.listen(port, function () {
    console.log('Gulp is running at : ' + port);
})

module.exports = app;