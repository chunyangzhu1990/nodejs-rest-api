////////////////////////////////////////////////////////////////////////////////
//
//  Author: Chunyang Zhu
//
//  Description: This file is setting up the gulp, and use it run the server and unit test
//               Server command:gulp
//               Unit test command: gulp test
//
////////////////////////////////////////////////////////////////////////////////




//using gulp to run the server
var gulp = require('gulp'),
    nodemon = require('gulp-nodemon');
gulpMocha = require('gulp-mocha');
env = require('gulp-env');
supertest = require('supertest');


gulp.task('default', function () {
    nodemon({
        script: 'app.js',
        ext: 'js',
        env: {
            PORT: 8000          //set up port = 8000
        },
        ignore: ['./node_modules/**']
    }).on('restart', function () {      //set up restart server once we update the code
        console.log('Restarting');
    });
});

// setting up testing environment using gulp
gulp.task('test', function () {
    env({vars: {ENV: 'Test'}});
    gulp.src('./test/**/*.js', {read: false}).pipe(gulpMocha({reporter: 'nyan'}))
});